// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GKlibrary.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT3_API UGKlibrary : 
	public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	//String addition class for my timer
	UFUNCTION(BlueprintCallable, Category = "Functions")
		static FText TimerCompiler(int msecs, int secs, int mins);
	
	
};
