// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT3_GKlibrary_generated_h
#error "GKlibrary.generated.h already included, missing '#pragma once' in GKlibrary.h"
#endif
#define MYPROJECT3_GKlibrary_generated_h

#define MyProject3_Source_MyProject3_GKlibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTimerCompiler) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_msecs); \
		P_GET_PROPERTY(UIntProperty,Z_Param_secs); \
		P_GET_PROPERTY(UIntProperty,Z_Param_mins); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=UGKlibrary::TimerCompiler(Z_Param_msecs,Z_Param_secs,Z_Param_mins); \
		P_NATIVE_END; \
	}


#define MyProject3_Source_MyProject3_GKlibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTimerCompiler) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_msecs); \
		P_GET_PROPERTY(UIntProperty,Z_Param_secs); \
		P_GET_PROPERTY(UIntProperty,Z_Param_mins); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=UGKlibrary::TimerCompiler(Z_Param_msecs,Z_Param_secs,Z_Param_mins); \
		P_NATIVE_END; \
	}


#define MyProject3_Source_MyProject3_GKlibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGKlibrary(); \
	friend struct Z_Construct_UClass_UGKlibrary_Statics; \
public: \
	DECLARE_CLASS(UGKlibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MyProject3"), NO_API) \
	DECLARE_SERIALIZER(UGKlibrary)


#define MyProject3_Source_MyProject3_GKlibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUGKlibrary(); \
	friend struct Z_Construct_UClass_UGKlibrary_Statics; \
public: \
	DECLARE_CLASS(UGKlibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MyProject3"), NO_API) \
	DECLARE_SERIALIZER(UGKlibrary)


#define MyProject3_Source_MyProject3_GKlibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGKlibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGKlibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGKlibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGKlibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGKlibrary(UGKlibrary&&); \
	NO_API UGKlibrary(const UGKlibrary&); \
public:


#define MyProject3_Source_MyProject3_GKlibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGKlibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGKlibrary(UGKlibrary&&); \
	NO_API UGKlibrary(const UGKlibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGKlibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGKlibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGKlibrary)


#define MyProject3_Source_MyProject3_GKlibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define MyProject3_Source_MyProject3_GKlibrary_h_11_PROLOG
#define MyProject3_Source_MyProject3_GKlibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject3_Source_MyProject3_GKlibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject3_Source_MyProject3_GKlibrary_h_15_RPC_WRAPPERS \
	MyProject3_Source_MyProject3_GKlibrary_h_15_INCLASS \
	MyProject3_Source_MyProject3_GKlibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject3_Source_MyProject3_GKlibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject3_Source_MyProject3_GKlibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject3_Source_MyProject3_GKlibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject3_Source_MyProject3_GKlibrary_h_15_INCLASS_NO_PURE_DECLS \
	MyProject3_Source_MyProject3_GKlibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject3_Source_MyProject3_GKlibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
