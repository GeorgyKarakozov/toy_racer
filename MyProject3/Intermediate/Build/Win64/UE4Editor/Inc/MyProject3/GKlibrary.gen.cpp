// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyProject3/GKlibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGKlibrary() {}
// Cross Module References
	MYPROJECT3_API UClass* Z_Construct_UClass_UGKlibrary_NoRegister();
	MYPROJECT3_API UClass* Z_Construct_UClass_UGKlibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MyProject3();
	MYPROJECT3_API UFunction* Z_Construct_UFunction_UGKlibrary_TimerCompiler();
// End Cross Module References
	void UGKlibrary::StaticRegisterNativesUGKlibrary()
	{
		UClass* Class = UGKlibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TimerCompiler", &UGKlibrary::execTimerCompiler },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics
	{
		struct GKlibrary_eventTimerCompiler_Parms
		{
			int32 msecs;
			int32 secs;
			int32 mins;
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_mins;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_secs;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_msecs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Text, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(GKlibrary_eventTimerCompiler_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_mins = { UE4CodeGen_Private::EPropertyClass::Int, "mins", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(GKlibrary_eventTimerCompiler_Parms, mins), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_secs = { UE4CodeGen_Private::EPropertyClass::Int, "secs", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(GKlibrary_eventTimerCompiler_Parms, secs), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_msecs = { UE4CodeGen_Private::EPropertyClass::Int, "msecs", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(GKlibrary_eventTimerCompiler_Parms, msecs), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_mins,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_secs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::NewProp_msecs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::Function_MetaDataParams[] = {
		{ "Category", "Functions" },
		{ "ModuleRelativePath", "GKlibrary.h" },
		{ "ToolTip", "String addition class for my timer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGKlibrary, "TimerCompiler", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04042401, sizeof(GKlibrary_eventTimerCompiler_Parms), Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGKlibrary_TimerCompiler()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGKlibrary_TimerCompiler_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGKlibrary_NoRegister()
	{
		return UGKlibrary::StaticClass();
	}
	struct Z_Construct_UClass_UGKlibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGKlibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MyProject3,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGKlibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGKlibrary_TimerCompiler, "TimerCompiler" }, // 1936692766
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGKlibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GKlibrary.h" },
		{ "ModuleRelativePath", "GKlibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGKlibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGKlibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGKlibrary_Statics::ClassParams = {
		&UGKlibrary::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UGKlibrary_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UGKlibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGKlibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGKlibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGKlibrary, 3651219710);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGKlibrary(Z_Construct_UClass_UGKlibrary, &UGKlibrary::StaticClass, TEXT("/Script/MyProject3"), TEXT("UGKlibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGKlibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
